import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';

export const TEST_PROVIDER = {
  provide: 'TEST_PROVIDER',
  inject: [ConfigService],
  useFactory: async (config: ConfigService) => {
    const MICRO_TEST_HOST = config.get<string>(
      'MICRO_TEST_HOST',
      'localhost'
    );
    const MICRO_TEST_PORT = config.get<number>(
      'MICRO_TEST_PORT',
      5090
    );
    console.log({ MICRO_TEST_HOST, MICRO_TEST_PORT });
    return ClientProxyFactory.create({
      transport: Transport.GRPC,
      options: {
        package: 'PACKAGE',
        url: `${MICRO_TEST_HOST}:${MICRO_TEST_PORT}`,
        protoPath: join(__dirname, 'proto/main.proto'),
        loader: { keepCase: true }
      }
    });
  }
};

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, ConfigService, TEST_PROVIDER]
})
export class AppModule { }
