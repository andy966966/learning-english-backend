import { VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    app.enableVersioning({
        type: VersioningType.URI,
        defaultVersion: '1'
    });

    app.setGlobalPrefix('api', {
        exclude: ['/']
    });

    const swaggerOptions = new DocumentBuilder()
        .setTitle('')
        .setDescription('')
        .setVersion('1')
        .addBearerAuth()
        .build();

    const swaggerDocument = SwaggerModule.createDocument(
        app,
        swaggerOptions
    );

    SwaggerModule.setup('docs', app, swaggerDocument);

    await app.listen(3000);
}
bootstrap();
