import { Controller, Get, Query, Req, Res } from '@nestjs/common';
import { ApiExtraModels, ApiOkResponse } from '@nestjs/swagger';
import { Request, Response } from 'express';
import {
    GetUserMicroRequestDto,
    GetUserMicroResponseDto
} from './app.dto';
import { AppService } from './app.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {}

    @Get('test')
    async getServerResponseMicro(@Query() data: { message: string }) {
        return this.appService.getServerResponseMicro(data);
    }

    @Get('user')
    @ApiOkResponse({ type: GetUserMicroResponseDto })
    @ApiExtraModels(GetUserMicroResponseDto)
    async getUserMicro(
        @Query() data: GetUserMicroRequestDto,
        @Req() req: Request,
        @Res({ passthrough: true }) res: Response
    ): Promise<GetUserMicroResponseDto> {
        // req: [
        //     '_readableState',
        //     '_events',
        //     '_eventsCount',
        //     '_maxListeners',
        //     'socket',
        //     'httpVersionMajor',
        //     'httpVersionMinor',
        //     'httpVersion',
        //     'complete',
        //     'rawHeaders',
        //     'rawTrailers',
        //     'aborted',
        //     'upgrade',
        //     'url',
        //     'method',
        //     'statusCode',
        //     'statusMessage',
        //     'client',
        //     '_consuming',
        //     '_dumped',
        //     'next',
        //     'baseUrl',
        //     'originalUrl',
        //     '_parsedUrl',
        //     'params',
        //     'query',
        //     'res',
        //     'body',
        //     'route'
        // ]
        const {
            httpVersionMajor,
            httpVersionMinor,
            httpVersion,
            method,
            rawHeaders
        } = req;
        console.log({
            httpVersionMajor,
            httpVersionMinor,
            httpVersion,
            method,
            rawHeaders
        });
        // res: [
        //     ('_events',
        //     '_eventsCount',
        //     '_maxListeners',
        //     'outputData',
        //     'outputSize',
        //     'writable',
        //     'destroyed',
        //     '_last',
        //     'chunkedEncoding',
        //     'shouldKeepAlive',
        //     'maxRequestsOnConnectionReached',
        //     '_defaultKeepAlive',
        //     'useChunkedEncodingByDefault',
        //     'sendDate',
        //     '_removedConnection',
        //     '_removedContLen',
        //     '_removedTE',
        //     'strictContentLength',
        //     '_contentLength',
        //     '_hasBody',
        //     '_trailer',
        //     'finished',
        //     '_headerSent',
        //     '_closed',
        //     'socket',
        //     '_header',
        //     '_keepAliveTimeout',
        //     '_onPendingData',
        //     'req',
        //     '_sent100',
        //     '_expect_continue',
        //     '_maxRequestsPerSocket',
        //     'locals',
        //     'statusCode')
        // ];
        return this.appService.getUserMicro(data);
    }

    @Get()
    getHello(): string {
        return this.appService.getHello();
    }
}
