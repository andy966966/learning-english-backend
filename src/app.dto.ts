import { ApiProperty } from '@nestjs/swagger';

export class GetUserMicroRequestDto {
    @ApiProperty({ description: 'ID' })
    id: string;
}

export class GetUserMicroResponseDto {
    @ApiProperty({ description: 'STATUS CODE' })
    statusCode: number;

    @ApiProperty({ description: 'ERROR' })
    error: string;

    @ApiProperty({ description: 'DATA' })
    data: string;

    @ApiProperty({ description: 'MESSAGE' })
    message: string;
}
