import { Inject, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ClientGrpc } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import {
    GetUserMicroRequestDto,
    GetUserMicroResponseDto
} from './app.dto';

interface ITest {
    getServerResponseMicro(bodyData: {
        message: string;
    }): Observable<unknown>;
}

interface IUser {
    getUserMicro(
        data: GetUserMicroRequestDto
    ): Observable<GetUserMicroResponseDto>;
}

@Injectable()
export class AppService {
    private Test: ITest;
    private User: IUser;

    constructor(
        @Inject('TEST_PROVIDER') private readonly client: ClientGrpc
    ) {
        this.Test = this.client.getService<ITest>('TestService');
        this.User = this.client.getService<IUser>('UserService');
        setTimeout(async () => {
            console.log(
                Object.keys(
                    client['grpcClients'][0]['TestService']['service']
                )
            );
            console.log(
                Object.keys(
                    client['grpcClients'][0]['UserService']['service']
                )
            );
            console.log(this.Test);
            console.log(this.User);
            await this.getServerResponseMicro({ message: 'test' });
            await this.getUserMicro({ id: '2345' });
        }, 100);
    }

    async getServerResponseMicro(data: { message: string }) {
        const res = await lastValueFrom(
            this.Test.getServerResponseMicro(data)
        );
        console.log({ res });
        return res;
    }

    async getUserMicro(
        data: GetUserMicroRequestDto
    ): Promise<GetUserMicroResponseDto> {
        const res = await lastValueFrom(this.User.getUserMicro(data));
        console.log({ res });
        return res;
    }

    getHello(): string {
        return 'Hello World!';
    }
}
